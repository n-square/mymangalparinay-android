package com.mymangalparinay.mymangalparinayapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.model.PhotoSlideModel;

import java.util.List;

/**
  Created by Jyoti on 10/1/2017.
 */

public class PhotoAdapter extends PagerAdapter {
    private Context context;
    private List<PhotoSlideModel> photoSlideModelList;


    public PhotoAdapter(Context context, List<PhotoSlideModel> photoSlideModelList) {
        this.context = context;
        this.photoSlideModelList = photoSlideModelList;
    }

    @Override
    public int getCount() {
        return photoSlideModelList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (View) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View viewItem = inflater.inflate(R.layout.item_view_photo_slider, container, false);

        ImageView ivUserPhoto = (ImageView) viewItem.findViewById(R.id.ivUserPhoto);

        ((ViewPager) container).addView(viewItem);

        ivUserPhoto.setImageResource(photoSlideModelList.get(position).getImage());
        return viewItem;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
