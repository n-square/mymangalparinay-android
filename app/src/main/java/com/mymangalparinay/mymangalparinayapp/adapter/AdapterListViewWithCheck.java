package com.mymangalparinay.mymangalparinayapp.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.model.CommonModelCheckBox;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 07-Dec-16.
 */
public class AdapterListViewWithCheck extends BaseAdapter {

    Context context;
    List<CommonModelCheckBox> commonModelCheckBoxes;
    CommonModelCheckBox itemModel;
    public SparseBooleanArray mCheckStates;

    public void setItem(List<CommonModelCheckBox> commonModelCheckBoxes) {
        this.commonModelCheckBoxes = commonModelCheckBoxes;
    }


    public AdapterListViewWithCheck(Context context, List<CommonModelCheckBox> commonModelCheckBoxes) {
        this.context = context;
        this.commonModelCheckBoxes = commonModelCheckBoxes;
        mCheckStates = new SparseBooleanArray(commonModelCheckBoxes.size());
    }

    @Override
    public int getCount() {
        return commonModelCheckBoxes.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        itemModel = commonModelCheckBoxes.get(i);
        ViewHolder viewHolder;


        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_view_list_with_check, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.cbChecked.setTag(i);
        viewHolder.rlCheck.setTag(i);
        viewHolder.cbChecked.setChecked(itemModel.isSelected());
        viewHolder.tvListItemName.setText(itemModel.getName());
        return view;

    }

    class ViewHolder {
        @BindView(R.id.cbChecked)
        CheckBox cbChecked;
        @BindView(R.id.tvListItemName)
        CustomTextView tvListItemName;
        @BindView(R.id.rlCheck)
        RelativeLayout rlCheck;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);


            rlCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    if (!commonModelCheckBoxes.get(pos).isSelected()) {
                        commonModelCheckBoxes.get(pos).setSelected(true);
                        cbChecked.setChecked(true);
                    } else {
                        commonModelCheckBoxes.get(pos).setSelected(false);
                        cbChecked.setChecked(false);

                    }
                }
            });
          /*  cbChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int poi = (Integer) buttonView.getTag();
                    commonModelCheckBoxes.get(poi).setSelected(buttonView.isChecked());
                }
            });*/


        }


    }


}
