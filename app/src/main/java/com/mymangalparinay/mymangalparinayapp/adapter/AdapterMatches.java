package com.mymangalparinay.mymangalparinayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.interfaces.OnClickDetailView;
import com.mymangalparinay.mymangalparinayapp.interfaces.OnClickViewPhoto;
import com.mymangalparinay.mymangalparinayapp.model.MatchesModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/12/2017.
 */

public class AdapterMatches extends BaseAdapter {
    private Context context;
    private OnClickDetailView onClickDetailView;
    private List<MatchesModel> machesModelList;
    private OnClickViewPhoto onClickViewPhoto;

    public AdapterMatches(Context context, List<MatchesModel> machesModelList) {
        this.context = context;
        this.machesModelList = machesModelList;
    }

    @Override
    public int getCount() {
        return machesModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MatchesModel matchesModel = machesModelList.get(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_view_matches_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.rlView.setTag(position);
        viewHolder.rlInterest.setTag(position);
        viewHolder.rlShortlist.setTag(position);
        viewHolder.iVProfilePhoto.setTag(position);
        viewHolder.iVProfilePhoto.setImageResource(matchesModel.getPhoto());

        viewHolder.tVUsername.setText(matchesModel.getName());
        viewHolder.tVUserID.setText(" ( " + matchesModel.getId() + ")");
        viewHolder.tvEducation.setText("Msc comp");
        viewHolder.tvAge.setText(matchesModel.getAge() + " Years");
        viewHolder.tvHeight.setText(matchesModel.getHeight() + "''");

        return convertView;
    }

    class ViewHolder implements View.OnClickListener {
        @BindView(R.id.iVProfilePhoto)
        ImageView iVProfilePhoto;
        @BindView(R.id.iVShortlist)
        ImageView iVShortlist;
        @BindView(R.id.iVInterest)
        ImageView iVInterest;

        @BindView(R.id.ivView)
        ImageView ivView;

        @BindView(R.id.tVShortlist)
        CustomTextView tVShortlist;
        @BindView(R.id.tVInterested)
        CustomTextView tVInterested;
        @BindView(R.id.tVSendSms)
        CustomTextView tVSendSms;
        @BindView(R.id.tvUsername)
        CustomTextView tVUsername;
        @BindView(R.id.tVUserID)
        CustomTextView tVUserID;
        @BindView(R.id.tvEducation)
        CustomTextView tvEducation;
        @BindView(R.id.tvAge)
        CustomTextView tvAge;
        @BindView(R.id.tvHeight)
        CustomTextView tvHeight;
        @BindView(R.id.rlShortlist)
        RelativeLayout rlShortlist;
        @BindView(R.id.rlInterest)
        RelativeLayout rlInterest;
        @BindView(R.id.rlView)
        RelativeLayout rlView;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);

            rlShortlist.setOnClickListener(this);
            rlInterest.setOnClickListener(this);
            rlView.setOnClickListener(this);
            iVProfilePhoto.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rlShortlist:
                    //
                    break;
                case R.id.rlInterest:
                    //
                    break;
                case R.id.rlView:
                    //
                    // context.startActivity(new Intent(context, ActivityViewProfile.class));
                    int position = (int) v.getTag();
                    onClickDetailView.onDetailView(position);

                    break;


                case R.id.iVProfilePhoto:
                    int i = (int) v.getTag();
                    onClickViewPhoto.onClickViewPhoto(i);
                    break;

            }


        }
    }

    public void setOnClickProfile(OnClickDetailView onClickDetailView) {
        this.onClickDetailView = onClickDetailView;
    }


    public void setOnClickPhoto(OnClickViewPhoto onClickPhoto) {
        this.onClickViewPhoto = onClickPhoto;
    }


}
