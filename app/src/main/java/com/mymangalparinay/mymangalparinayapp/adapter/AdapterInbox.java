package com.mymangalparinay.mymangalparinayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 3/30/2017.
 */

public class AdapterInbox extends BaseAdapter {
    Context context;

    public AdapterInbox(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_view_inbox, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvUserId.setText("MP12345");
        viewHolder.tvUsername.setText("Deepak Panchal");
        viewHolder.tvMessage.setText("Hello hi");
        viewHolder.bAccept.setTag(position);
        viewHolder.bDecline.setTag(position);
        return convertView;
    }


    class ViewHolder {
        @BindView(R.id.ivProfilePhoto)
        CircleImageView ivProfilePhoto;
        @BindView(R.id.tvUsername)
        CustomTextView tvUsername;
        @BindView(R.id.tvUserId)
        CustomTextView tvUserId;
        @BindView(R.id.tvMessage)
        CustomTextView tvMessage;
        @BindView(R.id.bAccept)
        CustomButtonView bAccept;
        @BindView(R.id.bDecline)
        CustomButtonView bDecline;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
