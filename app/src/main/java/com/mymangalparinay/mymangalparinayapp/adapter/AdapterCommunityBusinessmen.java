package com.mymangalparinay.mymangalparinayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 3/30/2017.
 8*/

public class AdapterCommunityBusinessmen extends BaseAdapter {
    Context context;

    public AdapterCommunityBusinessmen(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_view_comminity_businessman, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvName.setText("Deepak Panchal");
        viewHolder.tvPhone.setText("+919096693518");
        viewHolder.tvBusinessName.setText("Software Service Provider");
        viewHolder.tvAddress.setText("Nasik");
        return convertView;
    }


    class ViewHolder {
        @BindView(R.id.ivProfile)
        ImageView ivProfile;
        @BindView(R.id.tvName)
        CustomTextView tvName;
        @BindView(R.id.tvPhone)
        CustomTextView tvPhone;
        @BindView(R.id.tvAddress)
        CustomTextView tvAddress;

        @BindView(R.id.tvBusinessName)
        CustomTextView tvBusinessName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
