package com.mymangalparinay.mymangalparinayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 4/12/2017.
 **/

public class AdapterSpinnerArea extends BaseAdapter {
    private Context context;
    private List<String> areaList;

    public AdapterSpinnerArea(Context context, List<String> areaList) {
        this.context = context;
        this.areaList = areaList;
    }

    @Override
    public int getCount() {
        return areaList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String area = areaList.get(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_view_spinner, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvArea.setText(area);

        return convertView;
    }

    class ViewHolder {
        @BindView(R.id.tvArea)
        CustomTextView tvArea;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
