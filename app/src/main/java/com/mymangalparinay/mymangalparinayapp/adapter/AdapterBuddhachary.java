package com.mymangalparinay.mymangalparinayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 3/30/2017.
 */

public class AdapterBuddhachary extends BaseAdapter {
    Context context;

    public AdapterBuddhachary(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_view_buddhachary, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvName.setText("Nitin Borale");
        viewHolder.tvPhone.setText("+919850385253");
        viewHolder.tvAddress.setText("Ambad,Nasik");
        return convertView;
    }


    class ViewHolder {
        @BindView(R.id.ivProfile)
        ImageView civProfile;
        @BindView(R.id.tvName)
        CustomTextView tvName;
        @BindView(R.id.tvPhone)
        CustomTextView tvPhone;
        @BindView(R.id.tvAddress)
        CustomTextView tvAddress;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
