package com.mymangalparinay.mymangalparinayapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.model.NavItemModel;

import java.util.List;

/**
 * Created by user on 07-Dec-16.
 */
public class AdapterNav extends BaseAdapter {

    Context context;
    List<NavItemModel> navItemModels;

    public AdapterNav(Context context, List<NavItemModel> navItemModels) {
        this.context = context;
        this.navItemModels = navItemModels;
    }

    @Override
    public int getCount() {
        return navItemModels.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        NavItemModel itemModel = navItemModels.get(i);

        CustomTextView textView = null;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.adpater_nav_view, viewGroup, false);
        textView = (CustomTextView) view.findViewById(R.id.tvNavItem);
        textView.setText(itemModel.getNavItemName());
        return view;

    }
}
