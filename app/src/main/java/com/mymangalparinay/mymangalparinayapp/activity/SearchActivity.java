package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.media.MediaRouter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterListViewWithCheck;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterNav;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.model.CommonModelCheckBox;
import com.mymangalparinay.mymangalparinayapp.model.NavItemModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 4/9/2017.
 **/

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = SearchActivity.class.getSimpleName();

    @BindView(R.id.tvAge)
    CustomTextView tvAge;
    @BindView(R.id.tvHeight)
    CustomTextView tvHeight;


    @BindView(R.id.llAge)
    LinearLayout llAge;
    @BindView(R.id.llHeight)
    LinearLayout llHeight;
    @BindView(R.id.llMaritalStatus)
    LinearLayout llMaritalStatus;
    @BindView(R.id.llEducation)
    LinearLayout llEducation;

    @BindView(R.id.rgGender)
    RadioGroup rgGender;

    @BindView(R.id.rbMale)
    RadioButton rbMale;

    @BindView(R.id.bSearchNow)
    CustomButtonView bSearchNow;

    @BindView(R.id.etSearchById)
    CustomEditTextView etSearchById;

    @BindView(R.id.ivSearch)
    ImageView ivSearch;

    @BindView(R.id.drawerlay_religion)
    DrawerLayout drawerlay_religion;

    @BindView(R.id.navview_religion)
    NavigationView navview_religion;

    @BindView(R.id.rlAgeFilterMain)
    RelativeLayout rlAgeFilterMain;

    @BindView(R.id.rlListFilter)
    RelativeLayout rlListFilter;
    @BindView(R.id.sFromAge)
    AppCompatSpinner sFromAge;
    @BindView(R.id.sToAge)
    AppCompatSpinner sToAge;

    @BindView(R.id.appBar)
    Toolbar appBar;


    String fromAge = "";
    String toAge = "";
    String fromHieght = "";
    String toHieght = "";

    private List<NavItemModel> ageList;
    private List<NavItemModel> listHeight;


    AdapterNav adapterNav;
    boolean isAge;
    boolean isHeight;
    boolean isEducation;
    boolean isMaritalStatus;

    List<CommonModelCheckBox> listMeterialStatus;
    List<CommonModelCheckBox> listEducation;
    AdapterListViewWithCheck adapterListViewWithCheck;

    @BindView(R.id.left_drawer)
    ListView left_drawer;


    @BindView(R.id.etSearchCommonNavigation)
    CustomEditTextView etSearchCommonNavigation;
    @BindView(R.id.ivCloseDrawerList)
    ImageView ivCloseDrawerList;

    @BindView(R.id.ivCloseDrawer)
    ImageView ivCloseDrawer;
    @BindView(R.id.ivActionDone)
    ImageView ivActionDone;
    @BindView(R.id.tvTitleFilter)
    CustomTextView tvTitleFilter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        init();

    }

    private void init() {
        Log.d(TAG, "init: invoke");
        ButterKnife.bind(this);

        loadToolbar();
        llAge.setOnClickListener(this);
        llHeight.setOnClickListener(this);
        llMaritalStatus.setOnClickListener(this);
        llEducation.setOnClickListener(this);
        bSearchNow.setOnClickListener(this);
        sToAge.setOnItemSelectedListener(this);
        sFromAge.setOnItemSelectedListener(this);

        ivActionDone.setOnClickListener(this);
        ivCloseDrawer.setOnClickListener(this);
        ivCloseDrawerList.setOnClickListener(this);


        loadAge();
        loadHeight();
        loadMaterialStatus();
        loadEducation();


    }

    private void loadEducation() {
        Log.d(TAG, "loadEducation: ");
        listEducation = new ArrayList<>();
        listEducation.add(new CommonModelCheckBox("B.A", false));
        listEducation.add(new CommonModelCheckBox("B.com", false));
        listEducation.add(new CommonModelCheckBox("Bsc", false));
        listEducation.add(new CommonModelCheckBox("B.C.A", false));
        listEducation.add(new CommonModelCheckBox("B.B.A", false));
        listEducation.add(new CommonModelCheckBox("B.E", false));
        listEducation.add(new CommonModelCheckBox("M.A", false));
        listEducation.add(new CommonModelCheckBox("M.com", false));
        listEducation.add(new CommonModelCheckBox("MSc", false));
        listEducation.add(new CommonModelCheckBox("M.C.A", false));
        listEducation.add(new CommonModelCheckBox("M.B.A", false));
        listEducation.add(new CommonModelCheckBox("M.E", false));
        adapterListViewWithCheck = new AdapterListViewWithCheck(this, listEducation);
        left_drawer.setAdapter(adapterListViewWithCheck);
    }

    private void loadMaterialStatus() {
        Log.d(TAG, "loadMatrialStatus: ");
        listMeterialStatus = new ArrayList<>();
        listMeterialStatus.add(new CommonModelCheckBox("Never Married", false));
        listMeterialStatus.add(new CommonModelCheckBox("Widowed", false));
        listMeterialStatus.add(new CommonModelCheckBox("Divorced", false));
        listMeterialStatus.add(new CommonModelCheckBox("Awaiting Divorced", false));
        adapterListViewWithCheck = new AdapterListViewWithCheck(this, listMeterialStatus);
        left_drawer.setAdapter(adapterListViewWithCheck);
    }

    private void loadHeight() {
        Log.d(TAG, "loadHeight: ");
        listHeight = new ArrayList<>();
        listHeight.add(new NavItemModel("Select Height"));
        listHeight.add(new NavItemModel("4.1"));
        listHeight.add(new NavItemModel("4.2"));
        listHeight.add(new NavItemModel("4.3"));
        listHeight.add(new NavItemModel("4.4"));
        listHeight.add(new NavItemModel("4.5"));
        listHeight.add(new NavItemModel("4.6"));
        listHeight.add(new NavItemModel("4.7"));
        listHeight.add(new NavItemModel("4.8"));
        listHeight.add(new NavItemModel("4.9"));
        listHeight.add(new NavItemModel("4.10"));
        listHeight.add(new NavItemModel("4.11"));
        listHeight.add(new NavItemModel("5.1"));
        listHeight.add(new NavItemModel("5.2"));
        listHeight.add(new NavItemModel("5.3"));
        listHeight.add(new NavItemModel("5.4"));
        listHeight.add(new NavItemModel("5.5"));
        listHeight.add(new NavItemModel("5.6"));
        listHeight.add(new NavItemModel("5.7"));
        listHeight.add(new NavItemModel("5.8"));
        listHeight.add(new NavItemModel("5.9"));
        listHeight.add(new NavItemModel("5.10"));
        listHeight.add(new NavItemModel("5.11"));
        listHeight.add(new NavItemModel("6.1"));
        listHeight.add(new NavItemModel("6.2"));
        listHeight.add(new NavItemModel("6.3"));
        listHeight.add(new NavItemModel("6.3"));
        listHeight.add(new NavItemModel("6.3"));
        listHeight.add(new NavItemModel("6.3"));
        listHeight.add(new NavItemModel("6.3"));
        listHeight.add(new NavItemModel("6.3"));
        listHeight.add(new NavItemModel("6.3"));
        listHeight.add(new NavItemModel("6.3"));
        adapterNav = new AdapterNav(this, listHeight);
        sToAge.setAdapter(adapterNav);
        sFromAge.setAdapter(adapterNav);
    }

    private void loadAge() {
        Log.d(TAG, "loadAge: ");
        ageList = new ArrayList<>();
        ageList.add(new NavItemModel("Select Age"));
        ageList.add(new NavItemModel("18"));
        ageList.add(new NavItemModel("20"));
        ageList.add(new NavItemModel("21"));
        ageList.add(new NavItemModel("22"));
        ageList.add(new NavItemModel("23"));
        ageList.add(new NavItemModel("24"));
        ageList.add(new NavItemModel("25"));
        ageList.add(new NavItemModel("26"));
        ageList.add(new NavItemModel("27"));
        ageList.add(new NavItemModel("28"));
        ageList.add(new NavItemModel("29"));
        ageList.add(new NavItemModel("30"));
        ageList.add(new NavItemModel("31"));
        ageList.add(new NavItemModel("32"));
        ageList.add(new NavItemModel("33"));
        ageList.add(new NavItemModel("34"));
        ageList.add(new NavItemModel("35"));
        ageList.add(new NavItemModel("36"));
        ageList.add(new NavItemModel("37"));
        ageList.add(new NavItemModel("38"));
        ageList.add(new NavItemModel("39"));
        ageList.add(new NavItemModel("40"));
        ageList.add(new NavItemModel("41"));
        ageList.add(new NavItemModel("42"));
        adapterNav = new AdapterNav(this, ageList);
        sFromAge.setAdapter(adapterNav);
        sToAge.setAdapter(adapterNav);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llAge:
                isAge = true;
                isHeight = false;
                rlAgeFilterMain.setVisibility(View.VISIBLE);
                rlListFilter.setVisibility(View.GONE);
                drawerlay_religion.openDrawer(Gravity.RIGHT);
                loadAge();
                break;
            case R.id.llHeight:
                isAge = false;
                isHeight = true;
                rlAgeFilterMain.setVisibility(View.VISIBLE);
                rlListFilter.setVisibility(View.GONE);
                drawerlay_religion.openDrawer(Gravity.RIGHT);
                loadHeight();
                break;
            case R.id.llMaritalStatus:
                rlAgeFilterMain.setVisibility(View.GONE);
                rlListFilter.setVisibility(View.VISIBLE);
                drawerlay_religion.openDrawer(Gravity.RIGHT);
                loadMaterialStatus();
                break;
            case R.id.llEducation:
                rlAgeFilterMain.setVisibility(View.GONE);
                rlListFilter.setVisibility(View.VISIBLE);
                drawerlay_religion.openDrawer(Gravity.RIGHT);
                loadEducation();
                break;

            case R.id.bSearchNow:
                startActivity(new Intent(this, SearchResultActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                break;

            case R.id.ivActionDone:
                if (isAge) {
                    tvAge.setText(fromAge + " yrs" + " - " + toAge + " yrs");
                    //  NaviagtionClose();
                    drawerlay_religion.closeDrawers();

                } else if (isHeight) {
                    tvHeight.setText(fromHieght + "''" + " - " + toHieght + "''");
                    // NaviagtionClose();
                    drawerlay_religion.closeDrawers();
                }
                break;
            case R.id.ivCloseDrawer:
                drawerlay_religion.closeDrawers();
                break;
            case R.id.ivCloseDrawerList:
                drawerlay_religion.closeDrawers();
                break;


        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, HomeActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();

    }

    private void loadToolbar() {
        Log.d(TAG, "loadToolbar: ");
        setSupportActionBar(appBar);
        CustomTextView tvToolbarTitle = (CustomTextView) appBar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appBar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Search");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        AppCompatSpinner sFromAge = (AppCompatSpinner) parent;
        AppCompatSpinner sToAge = (AppCompatSpinner) parent;


        if (sFromAge.getId() == R.id.sFromAge) {
            //do this
            if (isAge) {
                Log.d(TAG, "onItemSelected: invoke");
                NavItemModel itemModel = ageList.get(position);
                fromAge = itemModel.getNavItemName();
                Log.d(TAG, "from Age: " + fromAge);

            } else if (isHeight) {
                NavItemModel itemModel = listHeight.get(position);
                fromHieght = itemModel.getNavItemName();
                Log.d(TAG, "fromHieght" + fromHieght);
            }

        } else if (sToAge.getId() == R.id.sToAge) {
            if (isAge) {
                Log.d(TAG, "onItemSelected: invoke to isAge");
                NavItemModel itemModel = ageList.get(position);
                toAge = itemModel.getNavItemName();
                Log.d(TAG, "to Age: " + toAge);
                //do this
            } else if (isHeight) {
                Log.d(TAG, "onItemSelected: invoke to isHeight");
                NavItemModel itemModel = listHeight.get(position);
                toHieght = itemModel.getNavItemName();
                Log.d(TAG, "to Age: " + toHieght);
            }

        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
