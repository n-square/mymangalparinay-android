package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.utils.Config;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/12/2017.
 */

public class LoginActitivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = LoginActitivity.class.getSimpleName();

    @BindView(R.id.et_email)
    CustomEditTextView et_email;
    @BindView(R.id.et_password)
    CustomEditTextView et_password;

    @BindView(R.id.btn_login)
    CustomButtonView btn_login;

  /*  @BindView(R.id.llSignUp)
    LinearLayout llSignUp;*/

    @BindView(R.id.tvForgotPasswod)
    CustomTextView tvForgotPasswod;

    @BindView(R.id.llSignUp)
    LinearLayout llSignUp;


    @BindView(R.id.tvDont)
    CustomTextView tvDont;
    RequestQueue requestQueue;

    String userName;
    String password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        tvDont.setText("Don't have an account?");
        btn_login.setOnClickListener(this);
        llSignUp.setOnClickListener(this);
        tvForgotPasswod.setOnClickListener(this);
        requestQueue = Volley.newRequestQueue(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                startActivity(new Intent(this, HomeActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
              /*  if (validateForm()) {
                    doLogIn();

                }
              */  break;

            case R.id.tvForgotPasswod:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                break;


            case R.id.llSignUp:
                startActivity(new Intent(this, SignUpActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                break;

        }
    }

    private void doLogIn() {
        StringRequest loginRequest = new StringRequest(Request.Method.POST, Config.LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", et_email.getText().toString().trim());
                params.put("password", et_password.getText().toString().trim());
                params.put("method", "login");
                Log.d(TAG, "getParams: " + params);
                return params;

            }
        };

        requestQueue.add(loginRequest);
    }


    private boolean validateForm() {
        Log.v(TAG, "validateForm() invoked");

        boolean isFormValid = true;

        initVarsFromForm();

        if (userName.length() <= 9) {
            et_email.setError("Enter  valid  mobile number");
            isFormValid = false;
        }


        if (password.length()<=5) {
            et_password.setError("Enter password at least 6 character");
            isFormValid = false;
        }

        return isFormValid;
    }

    private void initVarsFromForm() {
        password = et_password.getText().toString().trim();
        userName = et_email.getText().toString().trim();
    }

}
