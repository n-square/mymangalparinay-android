package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterNav;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.model.NavItemModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/25/2017.
 */

public class EditFamilyDetailsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static final String TAG = EditFamilyDetailsActivity.class.getSimpleName();

    @BindView(R.id.etFatherName)
    CustomEditTextView etFatherName;
    @BindView(R.id.etMotherName)
    CustomEditTextView etMotherName;
    @BindView(R.id.etFatherAnnualIncome)
    CustomEditTextView etFatherAnnualIncome;
    @BindView(R.id.etNoOfBrothers)
    CustomEditTextView etNoOfBrothers;
    @BindView(R.id.etNoOfBrothersMarried)
    CustomEditTextView etNoOfBrothersMarried;
    @BindView(R.id.etNoOfSisters)
    CustomEditTextView etNoOfSisters;
    @BindView(R.id.etNoOfSistersMarried)
    CustomEditTextView etNoOfSistersMarried;
    @BindView(R.id.etAboutFamily)
    CustomEditTextView etAboutFamily;
    @BindView(R.id.tvFamilyValue)
    CustomTextView tvFamilyValue;
    @BindView(R.id.tvFamilyType)
    CustomTextView tvFamilyType;
    @BindView(R.id.tvFamilyStatus)
    CustomTextView tvFamilyStatus;
    @BindView(R.id.tvFatherOccupation)
    CustomTextView tvFatherOccupation;
    @BindView(R.id.llFamilyValue)
    LinearLayout llFamilyValue;
    @BindView(R.id.llFamilyType)
    LinearLayout llFamilyType;
    @BindView(R.id.llFamilyStatus)
    LinearLayout llFamilyStatus;
    @BindView(R.id.llFatherOccupation)
    LinearLayout llFatherOccupation;
    @BindView(R.id.bSave)
    CustomButtonView bSave;
    @BindView(R.id.appbar)
    Toolbar appbar;
    @BindView(R.id.left_drawer)
    ListView left_drawer;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navView)
    NavigationView navView;


    boolean isFamilyStatus;
    boolean isFamilyType;
    boolean isFamilyValue;
    boolean isFatherOccupation;


    String familyStatus;
    String familyType;
    String familyValue;
    String fatherOccupation;

    AdapterNav adapterFamilyStatus;
    AdapterNav adapterFamilyType;
    AdapterNav adapterFamilyValue;
    AdapterNav adapterFatherOccupation;

    List<NavItemModel> listFamilyStatus;
    List<NavItemModel> listFamilyType;
    List<NavItemModel> listFamilyValue;
    List<NavItemModel> listFatherOccupation;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_family_details);
        init();


    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolBar();
        llFamilyStatus.setOnClickListener(this);
        llFamilyType.setOnClickListener(this);
        llFamilyValue.setOnClickListener(this);
        llFatherOccupation.setOnClickListener(this);
        bSave.setOnClickListener(this);
        left_drawer.setOnItemClickListener(this);
        doSetData();
        loadFamilyStatus();
        loadFamilyType();
        loadFamilyValues();
        loadFatherOccupation();


    }

    private void doSetData() {
        Log.d(TAG, "doSetData: ");
        etFatherName.setText("L K Pamchal");
        etMotherName.setText("B L Panchal");
        etFatherAnnualIncome.setText("100000");
        etNoOfBrothers.setText("1");
        etNoOfBrothersMarried.setText("no");
        etNoOfSisters.setText("2");
        etNoOfSistersMarried.setText("1");
        etAboutFamily.setText("good");
        tvFamilyValue.setText("Moderate");
        tvFamilyType.setText("Nuclear family");
        tvFamilyStatus.setText("Upper-middle class");
        tvFatherOccupation.setText("Employed");


    }

    private void loadToolBar() {
        Log.d(TAG, "loadToolBar: ");
        setSupportActionBar(appbar);
        CustomTextView tvToolbarTitle = (CustomTextView) appbar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appbar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Edit Family Details");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, EditProfileActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llFamilyStatus:
                drawerLayout.openDrawer(Gravity.RIGHT);
                isFamilyStatus = true;
                isFamilyType = false;
                isFamilyValue = false;
                isFatherOccupation = false;
                left_drawer.setAdapter(adapterFamilyStatus);
                break;
            case R.id.llFamilyType:
                drawerLayout.openDrawer(Gravity.RIGHT);
                isFamilyStatus = false;
                isFamilyType = true;
                isFamilyValue = false;
                isFatherOccupation = false;
                left_drawer.setAdapter(adapterFamilyType);
                break;
            case R.id.llFamilyValue:
                drawerLayout.openDrawer(Gravity.RIGHT);
                isFamilyStatus = false;
                isFamilyType = false;
                isFamilyValue = true;
                isFatherOccupation = false;
                left_drawer.setAdapter(adapterFamilyValue);
                break;
            case R.id.llFatherOccupation:
                drawerLayout.openDrawer(Gravity.RIGHT);
                isFamilyStatus = false;
                isFamilyType = false;
                isFamilyValue = false;
                isFatherOccupation = true;
                left_drawer.setAdapter(adapterFatherOccupation);
                break;
            case R.id.bSave:
                onBackPressed();
                break;

        }


    }

    private void loadFatherOccupation() {
        listFatherOccupation = new ArrayList<>();
        listFatherOccupation.add(new NavItemModel("Employed"));
        listFatherOccupation.add(new NavItemModel("Business woman"));
        listFatherOccupation.add(new NavItemModel("Professional"));
        listFatherOccupation.add(new NavItemModel("Retired"));
        listFatherOccupation.add(new NavItemModel("Home Maker"));
        listFatherOccupation.add(new NavItemModel("Passed Away"));
        adapterFatherOccupation = new AdapterNav(this, listFatherOccupation);
    }


    private void loadFamilyStatus() {
        listFamilyStatus = new ArrayList<>();
        listFamilyStatus.add(new NavItemModel("Middle Class"));
        listFamilyStatus.add(new NavItemModel("Upper-middle class"));
        listFamilyStatus.add(new NavItemModel("Lower Middle Class"));
        listFamilyStatus.add(new NavItemModel("High class"));
        listFamilyStatus.add(new NavItemModel("Rich/Affluent"));
        adapterFamilyStatus = new AdapterNav(this, listFamilyStatus);
    }

    private void loadFamilyType() {
        listFamilyType = new ArrayList<>();
        listFamilyType.add(new NavItemModel("Joint family"));
        listFamilyType.add(new NavItemModel("Nuclear family"));
        listFamilyType.add(new NavItemModel("Others"));
        adapterFamilyType = new AdapterNav(this, listFamilyType);
    }

    private void loadFamilyValues() {
        listFamilyValue = new ArrayList<>();
        listFamilyValue.add(new NavItemModel("Orthodox"));
        listFamilyValue.add(new NavItemModel("Traditional"));
        listFamilyValue.add(new NavItemModel("Moderate"));
        listFamilyValue.add(new NavItemModel("Liberal"));
        adapterFamilyValue = new AdapterNav(this, listFamilyValue);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (isFamilyStatus) {
            NavItemModel navItemModel = listFamilyStatus.get(position);
            familyStatus = navItemModel.getNavItemName();
            tvFamilyStatus.setText(familyStatus);
            drawerLayout.closeDrawers();
        } else if (isFamilyType) {
            NavItemModel navItemModel = listFamilyType.get(position);
            familyType = navItemModel.getNavItemName();
            tvFamilyType.setText(familyType);
            drawerLayout.closeDrawers();
        } else if (isFamilyValue) {
            NavItemModel navItemModel = listFamilyValue.get(position);
            familyValue = navItemModel.getNavItemName();
            tvFamilyValue.setText(familyValue);
            drawerLayout.closeDrawers();
        } else if (isFatherOccupation) {
            NavItemModel navItemModel = listFatherOccupation.get(position);
            fatherOccupation = navItemModel.getNavItemName();
            tvFatherOccupation.setText(fatherOccupation);
            drawerLayout.closeDrawers();
        }


    }
}
