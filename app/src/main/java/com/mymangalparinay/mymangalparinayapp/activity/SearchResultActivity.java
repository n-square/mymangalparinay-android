package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterMatches;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.interfaces.OnClickDetailView;
import com.mymangalparinay.mymangalparinayapp.model.MatchesModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Akash on 4/26/2017.
 **/

public class SearchResultActivity extends AppCompatActivity implements OnClickDetailView {
    private static final String TAG = SearchResultActivity.class.getSimpleName();

    @BindView(R.id.appBar)
    Toolbar appBar;

    @BindView(R.id.lvSearchResult)
    ListView lvSearchResult;

    AdapterMatches adapterMaches;

    List<MatchesModel> machesModelList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolbar();

        machesModelList = new ArrayList<>();
        adapterMaches = new AdapterMatches(this, machesModelList);
        lvSearchResult.setAdapter(adapterMaches);
        adapterMaches.setOnClickProfile(this);
        //loadData();

    }

    private void loadToolbar() {
        Log.d(TAG, "loadToolbar: ");
        setSupportActionBar(appBar);
        CustomTextView tvToolbarTitle = (CustomTextView) appBar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appBar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Search Result");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, SearchActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }


   /* private void loadData() {
        machesModelList.add(new MatchesModel("MP0001", "Amol Bansode", "Software Engg", "24", "5.2", "Nasik", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0002", "Sudrashan Ahire", "Prof.", "32", "5.5", "MAlegoan", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0003", "Prajkata Mhaske", "Msc", "32", "5.5", "Aurangabad", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0004", "Jayashri Sonawane", "ANM (Nurse)", "21", "5.5", "Nasik", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0005", "Pratibha Gangurde", "B.pharm", "21", "5.5", "Mumbai", R.drawable.ic_account_circle_black_24dp));
        adapterMaches.notifyDataSetChanged();

    }
*/

    @Override
    public void onDetailView(int posotion) {
        startActivity(new Intent(this, ActivityViewProfile.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }
}
