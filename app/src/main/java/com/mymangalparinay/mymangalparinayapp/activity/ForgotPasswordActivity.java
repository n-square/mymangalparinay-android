package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 4/11/2017.
**/

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = ForgotPasswordActivity.class.getSimpleName();

    @BindView(R.id.etPhone)
    CustomEditTextView etPhone;
    @BindView(R.id.bSubmit)
    CustomButtonView bSubmit;

    @BindView(R.id.appBar)
    Toolbar appBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolbar();
        bSubmit.setOnClickListener(this);


    }

    private void loadToolbar() {
        Log.d(TAG, "loadToolbar: ");
        setSupportActionBar(appBar);
        CustomTextView tvToolbarTitle = (CustomTextView) appBar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appBar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Forgot Password");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSubmit:
                doSumbit();
                break;


        }

    }

    private void doSumbit() {
        Log.d(TAG, "doSumbit: ");
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, LoginActitivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }
}
