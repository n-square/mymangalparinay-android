package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.PhotoAdapter;
import com.mymangalparinay.mymangalparinayapp.model.PhotoSlideModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
  Created by Jyoti on 10/1/2017.
 */

public class ActivityViewPhoto extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    private static final String TAG = ActivityViewPhoto.class.getSimpleName();
    @BindView(R.id.vpPhoto)
    ViewPager vpPhoto;
    @BindView(R.id.ivBack)
    ImageView ivBack;


    List<PhotoSlideModel> photoSlideModelList;
    PhotoAdapter photoAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        vpPhoto.addOnPageChangeListener(this);

        photoSlideModelList = new ArrayList<>();
        photoAdapter = new PhotoAdapter(this, photoSlideModelList);
        vpPhoto.setAdapter(photoAdapter);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        loadData();

    }

    private void loadData() {
        Log.d(TAG, "loadData: ");
        photoSlideModelList.add(new PhotoSlideModel(R.drawable.nitin));
        photoSlideModelList.add(new PhotoSlideModel(R.drawable.person));
        photoSlideModelList.add(new PhotoSlideModel(R.drawable.img));
        photoSlideModelList.add(new PhotoSlideModel(R.drawable.person3));
        photoSlideModelList.add(new PhotoSlideModel(R.drawable.person4));
        photoAdapter.notifyDataSetChanged();

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(this, HomeActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();


    }
}
