package com.mymangalparinay.mymangalparinayapp.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterNav;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.model.NavItemModel;
import com.mymangalparinay.mymangalparinayapp.utils.Config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static final String TAG = SignUpActivity.class.getSimpleName();

    @BindView(R.id.etFirstName)
    CustomEditTextView etFirstName;
    @BindView(R.id.etMiddleName)
    CustomEditTextView etMiddleName;
    @BindView(R.id.etLastName)
    CustomEditTextView etLastName;
    @BindView(R.id.etMobilenumber)
    CustomEditTextView etMobileNumber;
    @BindView(R.id.et_password)
    CustomEditTextView et_password;
    @BindView(R.id.et_email)
    CustomEditTextView et_email;
    @BindView(R.id.etAgentCode)
    CustomEditTextView etAgentCode;

    @BindView(R.id.tvDOB)
    CustomTextView tvDOB;


    @BindView(R.id.cbTerms)
    CheckBox cbTerms;
    @BindView(R.id.btn_signup)
    CustomButtonView btn_signup;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.tvProfileCreateFor)
    CustomTextView tvProfileCreateFor;

    @BindView(R.id.left_drawer)
    ListView left_drawer;


    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navView)
    NavigationView navView;


    @BindView(R.id.tvTerms)
    CustomTextView tvTerms;

    @BindView(R.id.appbar)
    Toolbar appbar;
    AdapterNav adapterNav;
    List<NavItemModel> listProfileCreater;
    private DatePickerDialog dpDOB;
    private SimpleDateFormat dateFormatter;
    String username;//mobile number as user name
    String f_name;
    String m_name;
    String l_name;
    String dob;
    String profile_for;
    String mobile_number;
    String password;
    String email;
    String gender;
    String mother_toungue;
    String religion;
    String client_code;
    String device_token;
    String agree_flag;


    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolbar();
        btn_signup.setOnClickListener(this);
        tvProfileCreateFor.setOnClickListener(this);
        tvDOB.setOnClickListener(this);
        tvTerms.setOnClickListener(this);
        left_drawer.setOnItemClickListener(this);
        listProfileCreater = new ArrayList<>();
        adapterNav = new AdapterNav(this, listProfileCreater);
        left_drawer.setAdapter(adapterNav);
        loadProfileData();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        setDate();
        requestQueue = Volley.newRequestQueue(this);

        loadListnerRadioButto();
        profile_for = "";
        agree_flag = "";

    }

    private void loadListnerRadioButto() {
        gender = "";
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rbMale) {
                    gender = "Male";
                } else {
                    gender = "Female";
                }

            }
        });

        cbTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    agree_flag = "1";
                } else {
                    agree_flag = "";
                }
            }
        });

    }

    private void loadToolbar() {
        Log.d(TAG, "loadToolbar: ");
        setSupportActionBar(appbar);
        CustomTextView tvToolbarTitle = (CustomTextView) appbar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appbar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Registration");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setDate() {
        Log.d(TAG, "setDate: ");
        dob = "";
        Calendar newCalendar = Calendar.getInstance();
        dpDOB = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dob = String.valueOf(year) + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                tvDOB.setText(dob);
                Log.d(TAG, "date: " + dob);

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    private void loadProfileData() {
        listProfileCreater.add(new NavItemModel("Daughter"));
        listProfileCreater.add(new NavItemModel("Sister"));
        listProfileCreater.add(new NavItemModel("Son"));
        listProfileCreater.add(new NavItemModel("Brother"));
        listProfileCreater.add(new NavItemModel("Myself"));
        listProfileCreater.add(new NavItemModel("Relative"));
        adapterNav.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_signup:
                //
//                startActivity(new Intent(this, HomeActivity.class));
//                overridePendingTransition(R.anim.enter, R.anim.exit);
//                finish();
                if (validateForm()) {
                    doSignUp();
                }
                break;
            case R.id.tvProfileCreateFor:
                //
                drawerLayout.openDrawer(Gravity.RIGHT);
                break;
            case R.id.tvDOB:
                dpDOB.show();
                break;

            case R.id.tvTerms:
                doTerms();
                break;


        }


    }

    private void doSignUp() {
        Log.d(TAG, "doSignUp: ");


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("password", password);///:123456
                params.put("method", "registration");///:registration
                params.put("profile_for", profile_for);///:sis
                params.put("gender", gender);///:male
                params.put("birth_date", dob);///:1993-12-24
                params.put("religion", "vdf.b");///:mom
                params.put("mother_toungue", "snvsn");///:rajstani
                params.put("mobile_number", mobile_number);///:1234
                params.put("email", email);///:d2gamil.com
                params.put("username", mobile_number);///:0123
                params.put("f_name", f_name);///:deep
                params.put("m_name", m_name);///:laxma
                params.put("l_name", l_name);///:pan
                params.put("agree_flag", agree_flag);///:agree
                params.put("client_code", etAgentCode.getText().toString().trim());///:123
                params.put("device_token", "dfvdfvd");///:fsgfjvbsvfjsgfsjgfjsgjf
                Log.d(TAG, "getParams:   " + params);
                return params;
            }
        };
        requestQueue.add(stringRequest);


    }

    private void doTerms() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_view_terms);
        dialog.setCancelable(false);

        CustomTextView tvContent = (CustomTextView) dialog.findViewById(R.id.tvContent);
        CustomButtonView bAgree = (CustomButtonView) dialog.findViewById(R.id.bAgree);
        tvContent.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum metus eget orci hendrerit, non cursus ipsum tristique. Aliquam euismod ullamcorper ex sed pretium. Mauris cursus tristique molestie. Nulla vitae ultrices sem, eget pellentesque ante. Pellentesque aliquet nunc eget felis aliquam rutrum. Maecenas molestie iaculis porttitor. Suspendisse imperdiet magna ac ipsum hendrerit, quis suscipit orci pellentesque. Etiam mollis iaculis diam vel efficitur. Nullam tincidunt vulputate odio in laoreet."
        );
        dialog.show();
        bAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        NavItemModel navItemModel = listProfileCreater.get(position);
        tvProfileCreateFor.setText(navItemModel.getNavItemName());
        profile_for = navItemModel.getNavItemName();
        drawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, LoginActitivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();

    }

    private boolean validateForm() {
        Log.v(TAG, "validateForm() invoked");

        boolean isFormValid = true;

        initVarsFromForm();


        if (f_name.equalsIgnoreCase("")) {
            etFirstName.setError("Enter First Name");
            isFormValid = false;
        }

        if (m_name.equalsIgnoreCase("")) {
            etMiddleName.setError("Enter Middle Name");
            isFormValid = false;
        }

        if (l_name.equalsIgnoreCase("")) {
            etLastName.setError("Enter Last Name");
            isFormValid = false;
        }


        if (username.length() <= 9) {
            etMobileNumber.setError("Enter Mobile number at least 10 digit");
            isFormValid = false;
        }

        if (password.length() <= 5) {
            et_password.setError("Enter Password at least 6 digits");
            isFormValid = false;
        }


        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("Enter valid email address");
            isFormValid = false;
        }

        if (gender.equalsIgnoreCase("")) {
            Toast.makeText(this, "Select your gender", Toast.LENGTH_SHORT).show();
            isFormValid = false;
        }

        if (dob.equalsIgnoreCase("")) {
            Toast.makeText(this, "Select your Date of Birth", Toast.LENGTH_SHORT).show();
            isFormValid = false;
        }

        if (profile_for.equalsIgnoreCase("")) {
            Toast.makeText(this, "Select Profile created for", Toast.LENGTH_SHORT).show();
            isFormValid = false;
        }

        if (agree_flag.equalsIgnoreCase("")) {
            Toast.makeText(this, "Please agree terms and condition", Toast.LENGTH_SHORT).show();
            isFormValid = false;
        }


       /* if (name.equalsIgnoreCase("")) {
            etEmail.setError("Name cannot be empty");
            isFormValid = false;
        }


        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Invalid Email Address");
            isFormValid = false;
        }
        if (password.equals("")) {
            etPassword.setError("Password cannot be empty");
            isFormValid = false;
        }*/

        return isFormValid;
    }

    private void initVarsFromForm() {
        username = etMobileNumber.getText().toString().trim();
        password = et_password.getText().toString().trim();
        f_name = etFirstName.getText().toString().trim();
        m_name = etMiddleName.getText().toString().trim();
        l_name = etLastName.getText().toString().trim();
        mobile_number = etMobileNumber.getText().toString().trim();
        password = et_password.getText().toString().trim();
        email = et_email.getText().toString().trim();
        client_code = et_email.getText().toString().trim();
        device_token = et_email.getText().toString().trim();
    }
}
