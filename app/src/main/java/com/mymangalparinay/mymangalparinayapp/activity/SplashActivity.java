package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mymangalparinay.mymangalparinayapp.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ADMIN on 3/22/2017.
 **/

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

/*                if (Sharepref.getCustomer_id(getApplicationContext()) == null) {
                    startActivity(new Intent(getApplicationContext(), Login_Activity.class));
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }*/
                startActivity(new Intent(getApplicationContext(), LoginActitivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();
            }
        }, 3000);

    }
}
