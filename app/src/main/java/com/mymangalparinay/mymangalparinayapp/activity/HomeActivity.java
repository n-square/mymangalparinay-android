package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.fragment.FragamentMailBox;
import com.mymangalparinay.mymangalparinayapp.fragment.FragmentCommunityBusinessmen;
import com.mymangalparinay.mymangalparinayapp.fragment.FragmentContactUs;
import com.mymangalparinay.mymangalparinayapp.fragment.FragmentFavouriteList;
import com.mymangalparinay.mymangalparinayapp.fragment.FragmentSetting;
import com.mymangalparinay.mymangalparinayapp.fragment.FragmentViewedMyProfile;
import com.mymangalparinay.mymangalparinayapp.fragment.MatchesFragment;
import com.mymangalparinay.mymangalparinayapp.fragment.OurBuaddhacharyFragment;
import com.mymangalparinay.mymangalparinayapp.utils.CustomTypefaceSpan;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 3/12/2017.
 * 8
 */

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = HomeActivity.class.getSimpleName();
    boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.appbar)
    Toolbar appbar;
    @BindView(R.id.navView)
    NavigationView navView;


    View holder;

    RelativeLayout rlEditProfile;
    CircleImageView ivProfile;
    CustomTextView tvUsername;
    CustomTextView tvUniqueId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        setSupportActionBar(appbar);

        //nav view
        holder = navView.getHeaderView(0);
        rlEditProfile = (RelativeLayout) holder.findViewById(R.id.rlEditProfile);
        ivProfile = (CircleImageView) holder.findViewById(R.id.ivProfile);
        tvUsername = (CustomTextView) holder.findViewById(R.id.tvUsername);
        tvUniqueId = (CustomTextView) holder.findViewById(R.id.tvUniqueId);
        tvUniqueId.setText("(MP001)");
        tvUsername.setText("Deepak Panchal");
        ivProfile.setImageDrawable(getResources().getDrawable(R.drawable.ic_account_circle_black_24dp));


        rlEditProfile.setOnClickListener(this);
        ivProfile.setOnClickListener(this);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, appbar, R.string.nav_drawer_open, R.string.nav_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this);
        Menu m = navView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            applyFontToMenuItem(mi);
        }


        getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new MatchesFragment()).commit();

    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface fonts = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/OpenSans-Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", fonts), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlEditProfile:
                startActivity(new Intent(this, EditProfileActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                drawerLayout.closeDrawers();
                break;

            case R.id.ivProfile:
                // startActivity(new Intent(this, EditProfileActivity.class));
                // finish();
                drawerLayout.closeDrawers();
                break;


        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.matches:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new MatchesFragment()).commit();
                drawerLayout.closeDrawers();
                break;

            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                drawerLayout.closeDrawers();
                break;

            case R.id.shoerlisted:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new FragmentFavouriteList()).commit();
                drawerLayout.closeDrawers();
                break;

            case R.id.setting:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new FragmentSetting()).commit();

                drawerLayout.closeDrawers();
                break;
            case R.id.mailbox:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new FragamentMailBox()).commit();
                drawerLayout.closeDrawers();
                break;


            case R.id.viewed:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new FragmentViewedMyProfile()).commit();

                drawerLayout.closeDrawers();

                break;


            case R.id.logout:
                startActivity(new Intent(this, LoginActitivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();
                drawerLayout.closeDrawers();

                break;


            case R.id.Our_Buddhacharyas:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new OurBuaddhacharyFragment()).commit();
                drawerLayout.closeDrawers();
                break;

            case R.id.CommunityBusinessmen:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new FragmentCommunityBusinessmen()).commit();
                drawerLayout.closeDrawers();
                break;


            case R.id.ContactUs:
                getSupportFragmentManager().beginTransaction().replace(R.id.flContain, new FragmentContactUs()).commit();
                drawerLayout.closeDrawers();
                break;


        }


        return true;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        //  Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        Snackbar.make(drawerLayout, "Please click BACK again to exit", Snackbar.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
