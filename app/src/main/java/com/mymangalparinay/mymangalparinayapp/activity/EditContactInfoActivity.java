package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/23/2017.
 * 8
 */

public class EditContactInfoActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = EditContactInfoActivity.class.getSimpleName();
    @BindView(R.id.etPrimaryNumber)
    CustomEditTextView etPrimaryNumber;
    @BindView(R.id.etSecondaryNumber)
    CustomEditTextView etSecondaryNumber;
    @BindView(R.id.etEmail)
    CustomEditTextView etEmail;
    @BindView(R.id.etAddress)
    CustomEditTextView etAddress;
    @BindView(R.id.bSave)
    CustomButtonView bSave;

    @BindView(R.id.appbar)
    Toolbar appbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info_edit);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolBar();
        etPrimaryNumber.setText("+919850385253");
        etSecondaryNumber.setText("+919898989898");
        etEmail.setText("Nitin@gmail.com");
        etAddress.setText("DGP Nagar");
        bSave.setOnClickListener(this);

    }

    private void loadToolBar() {
        setSupportActionBar(appbar);
        CustomTextView tvToolbarTitle = (CustomTextView) appbar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appbar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Edit Contact Information");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSave:
                startActivity(new Intent(this, EditProfileActivity.class));
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                finish();
                break;
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, EditProfileActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }
}
