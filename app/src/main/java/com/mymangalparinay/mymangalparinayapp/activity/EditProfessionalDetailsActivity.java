package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterNav;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.model.NavItemModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/25/2017.
 * =
 */

public class EditProfessionalDetailsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final String TAG = EditProfessionalDetailsActivity.class.getSimpleName();
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navView)
    NavigationView navView;
    @BindView(R.id.etCollage)
    CustomEditTextView etCollage;
    @BindView(R.id.etOccupationDetails)
    CustomEditTextView etOccupationDetails;
    @BindView(R.id.etEducationDetails)
    CustomEditTextView etEducationDetails;
    @BindView(R.id.etIncomeAmount)
    CustomEditTextView etIncomeAmount;
    @BindView(R.id.tvEducation)
    CustomTextView tvEducation;
    @BindView(R.id.tvOccupation)
    CustomTextView tvOccupation;
    @BindView(R.id.tvEmployedIn)
    CustomTextView tvEmployedIn;
    @BindView(R.id.tvIncomeType)
    CustomTextView tvIncomeType;
    @BindView(R.id.tvDesignation)
    CustomTextView tvDesignation;
    @BindView(R.id.llEmployedIn)
    LinearLayout llEmployedIn;
    @BindView(R.id.llEducation)
    LinearLayout llEducation;
    @BindView(R.id.llOccupation)
    LinearLayout llOccupation;
    @BindView(R.id.llIncomeType)
    LinearLayout llIncomeType;
    @BindView(R.id.llDesignation)
    LinearLayout llDesignation;
    @BindView(R.id.bSave)
    CustomButtonView bSave;
    @BindView(R.id.appbar)
    Toolbar appbar;
    @BindView(R.id.left_drawer)
    ListView left_drawer;


    boolean isEducation;
    boolean isEmployed;
    boolean isOccuaption;
    boolean isIncomeType;
    boolean isDesignation;

    String education;
    String employed;
    String occuaption;
    String incomeType;
    String designation;

    AdapterNav adapterEducation;
    AdapterNav adapterEmployed;
    AdapterNav adapterOccuaption;
    AdapterNav adapterIncomeType;
    AdapterNav adapterDesignation;

    List<NavItemModel> listEducation;
    List<NavItemModel> listEmployed;
    List<NavItemModel> listOccuaption;
    List<NavItemModel> listIncomeType;
    List<NavItemModel> listDesignation;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profenational_details);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolBar();
        llEmployedIn.setOnClickListener(this);
        llEducation.setOnClickListener(this);
        llOccupation.setOnClickListener(this);
        llIncomeType.setOnClickListener(this);
        llDesignation.setOnClickListener(this);
        bSave.setOnClickListener(this);
        left_drawer.setOnItemClickListener(this);
        loadData();

        loadEducation();
        loadEmployedIn();
        loadIncomeType();
        loadOccupation();
        loadDesignation();

    }

    private void loadOccupation() {
        listOccuaption = new ArrayList<>();
        listOccuaption.add(new NavItemModel("Service"));
        listOccuaption.add(new NavItemModel("Business"));
        adapterOccuaption = new AdapterNav(this, listOccuaption);
    }

    private void loadDesignation() {
        listDesignation = new ArrayList<>();
        listDesignation.add(new NavItemModel("Admin"));
        listDesignation.add(new NavItemModel("Manager"));
        listDesignation.add(new NavItemModel("Officer"));
        listDesignation.add(new NavItemModel("Software Professional"));
        listDesignation.add(new NavItemModel("Executive"));
        adapterDesignation = new AdapterNav(this, listDesignation);

    }

    private void loadIncomeType() {
        Log.d(TAG, "loadIncomeType: ");
        listIncomeType = new ArrayList<>();
        listIncomeType.add(new NavItemModel("Monthly"));
        listIncomeType.add(new NavItemModel("Annual"));
        adapterIncomeType = new AdapterNav(this, listIncomeType);


    }

    private void loadEmployedIn() {
        Log.d(TAG, "loadEmployedIn: ");
        listEmployed = new ArrayList<>();
        listEmployed.add(new NavItemModel("Government"));
        listEmployed.add(new NavItemModel("Defence"));
        listEmployed.add(new NavItemModel("Private"));
        listEmployed.add(new NavItemModel("Business"));
        listEmployed.add(new NavItemModel("Self Employed"));
        adapterEmployed = new AdapterNav(this, listEmployed);
    }

    private void loadEducation() {
        listEducation = new ArrayList<>();
        listEducation.add(new NavItemModel("B.A"));
        listEducation.add(new NavItemModel("B.com"));
        listEducation.add(new NavItemModel("Bsc"));
        listEducation.add(new NavItemModel("B.C.A"));
        listEducation.add(new NavItemModel("B.B.A"));
        listEducation.add(new NavItemModel("B.E"));
        listEducation.add(new NavItemModel("M.A"));
        listEducation.add(new NavItemModel("M.com"));
        listEducation.add(new NavItemModel("MSc"));
        listEducation.add(new NavItemModel("M.C.A"));
        listEducation.add(new NavItemModel("M.B.A"));
        listEducation.add(new NavItemModel("M.E"));
        adapterEducation = new AdapterNav(this, listEducation);

    }

    private void loadData() {
        etCollage.setText("KK wagh");
        etOccupationDetails.setText("");
        etEducationDetails.setText("Comp. Engg");
        etIncomeAmount.setText("80000");
        tvEducation.setText("B.C.A");
        tvOccupation.setText("Engineer");
        tvEmployedIn.setText("Private");
        tvIncomeType.setText("Annul");
        tvDesignation.setText("Software Engg");

    }

    private void loadToolBar() {
        Log.d(TAG, "loadToolBar: ");
        setSupportActionBar(appbar);
        CustomTextView tvToolbarTitle = (CustomTextView) appbar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appbar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Edit Professional Details");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, EditProfileActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llEmployedIn:
                drawerLayout.openDrawer(Gravity.RIGHT);
                isEducation = false;
                isEmployed = true;
                isOccuaption = false;
                isIncomeType = false;
                isDesignation = false;
                left_drawer.setAdapter(adapterEmployed);
                break;
            case R.id.llEducation:
                drawerLayout.openDrawer(Gravity.RIGHT);

                isEducation = true;
                isEmployed = false;
                isOccuaption = false;
                isIncomeType = false;
                isDesignation = false;
                left_drawer.setAdapter(adapterEducation);
                break;
            case R.id.llOccupation:
                drawerLayout.openDrawer(Gravity.RIGHT);
                isEducation = false;
                isEmployed = false;
                isOccuaption = true;
                isIncomeType = false;
                isDesignation = false;
                left_drawer.setAdapter(adapterOccuaption);
                break;
            case R.id.llIncomeType:
                drawerLayout.openDrawer(Gravity.RIGHT);
                isEducation = false;
                isEmployed = false;
                isOccuaption = false;
                isIncomeType = true;
                isDesignation = false;
                left_drawer.setAdapter(adapterIncomeType);

                break;
            case R.id.llDesignation:
                drawerLayout.openDrawer(Gravity.RIGHT);

                isEducation = false;
                isEmployed = false;
                isOccuaption = false;
                isIncomeType = false;
                isDesignation = true;
                left_drawer.setAdapter(adapterDesignation);
                break;
            case R.id.bSave:
                onBackPressed();
                break;


        }


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (isEducation) {
            NavItemModel navItemModel = listEducation.get(position);
            education = navItemModel.getNavItemName();
            tvEducation.setText(education);
            drawerLayout.closeDrawers();

        } else if (isEmployed) {
            NavItemModel navItemModel = listEmployed.get(position);
            employed = navItemModel.getNavItemName();
            tvEmployedIn.setText(employed);
            drawerLayout.closeDrawers();

        } else if (isOccuaption) {
            NavItemModel navItemModel = listOccuaption.get(position);
            occuaption = navItemModel.getNavItemName();
            tvOccupation.setText(occuaption);
            drawerLayout.closeDrawers();

        } else if (isIncomeType) {
            NavItemModel navItemModel = listIncomeType.get(position);
            incomeType = navItemModel.getNavItemName();
            tvIncomeType.setText(incomeType);
            drawerLayout.closeDrawers();

        } else if (isDesignation) {
            NavItemModel navItemModel = listDesignation.get(position);
            designation = navItemModel.getNavItemName();
            tvDesignation.setText(designation);
            drawerLayout.closeDrawers();
        }
    }
}
