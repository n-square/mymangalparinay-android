package com.mymangalparinay.mymangalparinayapp.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterNav;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.model.NavItemModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/20/2017.
 **/

public class EditBasicDetailsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final String TAG = EditBasicDetailsActivity.class.getSimpleName();
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navView)
    NavigationView navView;

    @BindView(R.id.etFistName)
    CustomEditTextView etFistName;

    @BindView(R.id.etMiddleName)
    CustomEditTextView etMiddleName;

    @BindView(R.id.etLastName)
    CustomEditTextView etLastName;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.tvAge)
    CustomTextView tvAge;
    @BindView(R.id.tvHeight)
    CustomTextView tvHeight;
    @BindView(R.id.tvWeightl)
    CustomTextView tvWeightl;
    @BindView(R.id.tvMaterialStatus)
    CustomTextView tvMaterialStatus;
    @BindView(R.id.tvPhysicalStatus)
    CustomTextView tvPhysicalStatus;
    @BindView(R.id.tvBodyType)
    CustomTextView tvBodyType;
    @BindView(R.id.tvProfileCreateBy)
    CustomTextView tvProfileCreateBy;
    @BindView(R.id.tvEatingHabits)
    CustomTextView tvEatingHabits;
    @BindView(R.id.tvDrinkingHabits)
    CustomTextView tvDrinkingHabits;
    @BindView(R.id.tvSmokingHabits)
    CustomTextView tvSmokingHabits;
    @BindView(R.id.bSave)
    CustomButtonView bSave;

    @BindView(R.id.tvComplexion)
    CustomTextView tvComplexion;

    @BindView(R.id.left_drawer)
    ListView left_drawer;

    @BindView(R.id.llAge)
    LinearLayout llAge;
    @BindView(R.id.llHeight)
    LinearLayout llHeight;
    @BindView(R.id.llWeight)
    LinearLayout llWeight;
    @BindView(R.id.llMStatus)
    LinearLayout llMStatus;
    @BindView(R.id.llPhysicalStatus)
    LinearLayout llPhysicalStatus;
    @BindView(R.id.llBodyType)
    LinearLayout llBodyType;
    @BindView(R.id.llComplexion)
    LinearLayout llComplexion;
    @BindView(R.id.llProfileCreateBy)
    LinearLayout llProfileCreateBy;
    @BindView(R.id.llEatingHabits)
    LinearLayout llEatingHabits;
    @BindView(R.id.llDrinkingHabits)
    LinearLayout llDrinkingHabits;
    @BindView(R.id.llSmokingHabits)
    LinearLayout llSmokingHabits;

    @BindView(R.id.appbar)
    Toolbar appbar;


    AdapterNav adapterHiegth;
    AdapterNav adapterWeigth;
    AdapterNav adapterPhysicalStaus;
    AdapterNav adapterBodyType;
    AdapterNav adapterProfileCreate;
    AdapterNav adapterSmoking;
    AdapterNav adapterDrinking;
    AdapterNav adapterComplexion;
    AdapterNav adapterEating;
    AdapterNav adapterMaterial;


    boolean isHeight;
    boolean isWeigth;
    boolean isMeterialStatus;
    boolean isPhysicalStatus;
    boolean isBodyType;
    boolean isProfileCreateBy;
    boolean isEditingHabit;
    boolean isDrinkingHabit;
    boolean isSmokingHabit;
    boolean isComplexion;

    String age;
    String height;
    String weigth;
    String meterialStatus;
    String physicalStatus;
    String bodyType;
    String profileCreateBy;
    String editingHabit;
    String drinkingHabit;
    String smokingHabit;
    String complexion;


    List<NavItemModel> listHeigth;
    List<NavItemModel> listWeigth;
    List<NavItemModel> listMeterialStatus;
    List<NavItemModel> listPhysicalStatus;
    List<NavItemModel> listBodyType;
    List<NavItemModel> listProfileCreateBy;
    List<NavItemModel> listEditingHabit;
    List<NavItemModel> listDrinkingHabit;
    List<NavItemModel> listSmokingHabit;
    List<NavItemModel> listComplexion;


    private DatePickerDialog dpDOB;
    String dob;
    private SimpleDateFormat dateFormatter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_basic_details);
        init();
    }

    private void init() {
        Log.d(TAG, "init: invoke");
        ButterKnife.bind(this);
        loadToolBar();
        llAge.setOnClickListener(this);
        llHeight.setOnClickListener(this);
        llWeight.setOnClickListener(this);
        llMStatus.setOnClickListener(this);
        llPhysicalStatus.setOnClickListener(this);
        llBodyType.setOnClickListener(this);
        llComplexion.setOnClickListener(this);
        llProfileCreateBy.setOnClickListener(this);
        llEatingHabits.setOnClickListener(this);
        llDrinkingHabits.setOnClickListener(this);
        llSmokingHabits.setOnClickListener(this);
        left_drawer.setOnItemClickListener(this);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        setData();

        doAge();
        loadHeight();
        loadWeight();
        loadMaterialStatus();
        loadPhysicalStatus();
        loadEditingHabit();
        loadDrinkingHabit();
        loadSmokingHabit();
        loadBodyType();
        loadComplex();
        loadProfileCreateBy();

    }

    private void setData() {
        Log.d(TAG, "setData: ");
        etFistName.setText("Deepak");
        etMiddleName.setText("L");
        etLastName.setText("Panchal");
        tvAge.setText("22-3-2012");
        tvHeight.setText("5.2''");
        tvWeightl.setText("63");
        tvMaterialStatus.setText("Never Married");
        tvPhysicalStatus.setText("Normal");
        tvBodyType.setText("Slim");
        tvEatingHabits.setText("Vegetarian");
        tvDrinkingHabits.setText("No");
        tvSmokingHabits.setText("No");
        tvComplexion.setText("Wheatish");
        tvProfileCreateBy.setText("Brother");
    }

    private void loadProfileCreateBy() {
        listProfileCreateBy = new ArrayList<>();
        listProfileCreateBy.add(new NavItemModel("Daughter"));
        listProfileCreateBy.add(new NavItemModel("Sister"));
        listProfileCreateBy.add(new NavItemModel("Brother"));
        listProfileCreateBy.add(new NavItemModel("Myself"));
        listProfileCreateBy.add(new NavItemModel("Relative"));
        listProfileCreateBy.add(new NavItemModel("Friend"));
        listProfileCreateBy.add(new NavItemModel("Son"));
        adapterProfileCreate = new AdapterNav(this, listProfileCreateBy);


    }

    private void loadComplex() {
        listComplexion = new ArrayList<>();
        listComplexion.add(new NavItemModel("Very Fair"));
        listComplexion.add(new NavItemModel("Fair"));
        listComplexion.add(new NavItemModel("Wheatish"));
        listComplexion.add(new NavItemModel("Wheatish brown"));
        listComplexion.add(new NavItemModel("Dark"));
        adapterComplexion = new AdapterNav(this, listComplexion);


    }

    private void loadBodyType() {
        Log.d(TAG, "loadBodyType: ");
        listBodyType = new ArrayList<>();
        listBodyType.add(new NavItemModel("Average"));
        listBodyType.add(new NavItemModel("Athletic"));
        listBodyType.add(new NavItemModel("Slim"));
        listBodyType.add(new NavItemModel("Heavy"));
        adapterBodyType = new AdapterNav(this, listBodyType);

    }

    private void loadSmokingHabit() {
        Log.d(TAG, "loadSmokingHabit: ");
        listSmokingHabit = new ArrayList<>();
        listSmokingHabit.add(new NavItemModel("No"));
        listSmokingHabit.add(new NavItemModel("occasionally"));
        listSmokingHabit.add(new NavItemModel("Yes"));
        adapterSmoking = new AdapterNav(this, listSmokingHabit);


    }

    private void loadDrinkingHabit() {
        Log.d(TAG, "loadDrinkingHabit: ");
        listDrinkingHabit = new ArrayList<>();
        listDrinkingHabit.add(new NavItemModel("No"));
        listDrinkingHabit.add(new NavItemModel("occasionally"));
        listDrinkingHabit.add(new NavItemModel("Yes"));
        adapterDrinking = new AdapterNav(this, listDrinkingHabit);


    }

    private void loadEditingHabit() {
        listEditingHabit = new ArrayList<>();
        listEditingHabit.add(new NavItemModel("Vegetarian"));
        listEditingHabit.add(new NavItemModel("Non-Vegetarian"));
        adapterEating = new AdapterNav(this, listEditingHabit);


    }

    private void loadPhysicalStatus() {
        listPhysicalStatus = new ArrayList<>();
        listPhysicalStatus.add(new NavItemModel("Normal"));
        listPhysicalStatus.add(new NavItemModel("Physically challenged"));
        adapterPhysicalStaus = new AdapterNav(this, listPhysicalStatus);


    }

    private void loadMaterialStatus() {
        Log.d(TAG, "loadMaterialStatus: ");
        listMeterialStatus = new ArrayList<>();
        listMeterialStatus.add(new NavItemModel("Never Married"));
        listMeterialStatus.add(new NavItemModel("Widowed"));
        listMeterialStatus.add(new NavItemModel("Divorced"));
        listMeterialStatus.add(new NavItemModel("Awaiting Divorced"));
        adapterMaterial = new AdapterNav(this, listMeterialStatus);

    }

    private void loadWeight() {
        Log.d(TAG, "loadWeight: ");
        listWeigth = new ArrayList<>();
        listWeigth.add(new NavItemModel("30"));
        listWeigth.add(new NavItemModel("31"));
        listWeigth.add(new NavItemModel("33"));
        listWeigth.add(new NavItemModel("34"));
        listWeigth.add(new NavItemModel("35"));
        listWeigth.add(new NavItemModel("36"));
        listWeigth.add(new NavItemModel("37"));
        listWeigth.add(new NavItemModel("38"));
        listWeigth.add(new NavItemModel("39"));
        listWeigth.add(new NavItemModel("40"));
        listWeigth.add(new NavItemModel("41"));
        listWeigth.add(new NavItemModel("42"));
        listWeigth.add(new NavItemModel("43"));
        listWeigth.add(new NavItemModel("44"));
        listWeigth.add(new NavItemModel("45"));
        listWeigth.add(new NavItemModel("46"));
        listWeigth.add(new NavItemModel("47"));
        listWeigth.add(new NavItemModel("48"));
        listWeigth.add(new NavItemModel("50"));
        adapterWeigth = new AdapterNav(this, listWeigth);


    }

    private void loadHeight() {
        Log.d(TAG, "lodHeight: ");
        listHeigth = new ArrayList<>();
        listHeigth.add(new NavItemModel("4.2"));
        listHeigth.add(new NavItemModel("4.3"));
        listHeigth.add(new NavItemModel("4.4"));
        listHeigth.add(new NavItemModel("4.5"));
        listHeigth.add(new NavItemModel("4.6"));
        listHeigth.add(new NavItemModel("4.7"));
        listHeigth.add(new NavItemModel("4.8"));
        listHeigth.add(new NavItemModel("4.9"));
        listHeigth.add(new NavItemModel("4.10"));
        listHeigth.add(new NavItemModel("4.11"));
        listHeigth.add(new NavItemModel("5.1"));
        listHeigth.add(new NavItemModel("5.2"));
        listHeigth.add(new NavItemModel("5.3"));
        listHeigth.add(new NavItemModel("5.4"));
        adapterHiegth = new AdapterNav(this, listHeigth);


    }

    private void doAge() {
        Log.d(TAG, "doAge: ");
        Calendar newCalendar = Calendar.getInstance();
        dpDOB = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tvAge.setText(dateFormatter.format(newDate.getTime()));
                dob = dateFormatter.format(newDate.getTime());
                Log.d(TAG, "date: " + dob);

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.llAge:
                //
                dpDOB.show();
                break;
            case R.id.llHeight:
                isHeight = true;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterHiegth);
                drawerLayout.openDrawer(Gravity.RIGHT);

                break;
            case R.id.llWeight:
                //
                isWeigth = true;
                isHeight = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterWeigth);
                drawerLayout.openDrawer(Gravity.RIGHT);
                break;
            case R.id.llMStatus:
                //
                isHeight = false;
                isWeigth = false;
                isMeterialStatus = true;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterMaterial);
                drawerLayout.openDrawer(Gravity.RIGHT);


                break;
            case R.id.llPhysicalStatus:
                //
                isHeight = false;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = true;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterPhysicalStaus);
                drawerLayout.openDrawer(Gravity.RIGHT);

                break;
            case R.id.llBodyType:
                //
                isHeight = false;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = true;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterBodyType);
                drawerLayout.openDrawer(Gravity.RIGHT);

                break;
            case R.id.llComplexion:
                //
                isHeight = false;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = true;
                left_drawer.setAdapter(adapterComplexion);
                drawerLayout.openDrawer(Gravity.RIGHT);

                break;
            case R.id.llProfileCreateBy:
                //

                isHeight = false;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = true;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterProfileCreate);
                drawerLayout.openDrawer(Gravity.RIGHT);


                break;
            case R.id.llEatingHabits:
                //
                isHeight = false;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = true;
                isDrinkingHabit = false;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterEating);
                drawerLayout.openDrawer(Gravity.RIGHT);

                break;
            case R.id.llDrinkingHabits:
                //
                isHeight = false;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = true;
                isSmokingHabit = false;
                isComplexion = false;
                left_drawer.setAdapter(adapterDrinking);
                drawerLayout.openDrawer(Gravity.RIGHT);

                break;
            case R.id.llSmokingHabits:
                //
                isHeight = false;
                isWeigth = false;
                isMeterialStatus = false;
                isPhysicalStatus = false;
                isBodyType = false;
                isProfileCreateBy = false;
                isEditingHabit = false;
                isDrinkingHabit = false;
                isSmokingHabit = true;
                isComplexion = false;
                left_drawer.setAdapter(adapterSmoking);
                drawerLayout.openDrawer(Gravity.RIGHT);

                break;
            case R.id.bSave:
                //
                break;


        }


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (isHeight) {
            NavItemModel navItemModel = listHeigth.get(position);
            height = navItemModel.getNavItemName();
            tvHeight.setText(height);
            drawerLayout.closeDrawers();

        } else if (isWeigth) {
            NavItemModel navItemModel = listWeigth.get(position);
            weigth = navItemModel.getNavItemName();
            tvWeightl.setText(weigth);
            drawerLayout.closeDrawers();

        } else if (isMeterialStatus) {
            NavItemModel navItemModel = listMeterialStatus.get(position);
            meterialStatus = navItemModel.getNavItemName();
            tvMaterialStatus.setText(meterialStatus);
            drawerLayout.closeDrawers();


        } else if (isPhysicalStatus) {
            NavItemModel navItemModel = listPhysicalStatus.get(position);

            physicalStatus = navItemModel.getNavItemName();
            tvPhysicalStatus.setText(physicalStatus);
            drawerLayout.closeDrawers();


        } else if (isBodyType) {
            NavItemModel navItemModel = listBodyType.get(position);
            bodyType = navItemModel.getNavItemName();
            tvBodyType.setText(bodyType);
            drawerLayout.closeDrawers();

        } else if (isProfileCreateBy) {
            NavItemModel navItemModel = listProfileCreateBy.get(position);
            profileCreateBy = navItemModel.getNavItemName();
            tvProfileCreateBy.setText(profileCreateBy);

            drawerLayout.closeDrawers();

        } else if (isEditingHabit) {
            NavItemModel navItemModel = listEditingHabit.get(position);
            editingHabit = navItemModel.getNavItemName();
            tvEatingHabits.setText(editingHabit);

            drawerLayout.closeDrawers();

        } else if (isDrinkingHabit) {
            NavItemModel navItemModel = listDrinkingHabit.get(position);
            drinkingHabit = navItemModel.getNavItemName();
            tvDrinkingHabits.setText(drinkingHabit);

            drawerLayout.closeDrawers();

        } else if (isSmokingHabit) {
            NavItemModel navItemModel = listSmokingHabit.get(position);
            smokingHabit = navItemModel.getNavItemName();
            tvSmokingHabits.setText(smokingHabit);
            drawerLayout.closeDrawers();


        } else if (isComplexion) {
            NavItemModel navItemModel = listComplexion.get(position);
            complexion = navItemModel.getNavItemName();
            tvComplexion.setText(complexion);
            drawerLayout.closeDrawers();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, EditProfileActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }

    private void loadToolBar() {
        Log.d(TAG, "loadToolBar: ");
        setSupportActionBar(appbar);
        CustomTextView tvToolbarTitle = (CustomTextView) appbar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appbar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Edit Basic Details");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


}
