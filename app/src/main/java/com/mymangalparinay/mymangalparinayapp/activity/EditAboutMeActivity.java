package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/19/2017.
 */

public class EditAboutMeActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = EditAboutMeActivity.class.getSimpleName();

    @BindView(R.id.bSave)
    CustomButtonView bSave;
    @BindView(R.id.etAboutMe)
    CustomEditTextView etAboutMe;

    @BindView(R.id.appBar)
    Toolbar appBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_about_me);
        init();


    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolBar();
        bSave.setOnClickListener(this);

    }

    private void loadToolBar() {
        setSupportActionBar(appBar);
        CustomTextView tvToolbarTitle = (CustomTextView) appBar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appBar.findViewById(R.id.ivBack);
        tvToolbarTitle.setText("Edit About Me");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSave:
                //
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, EditProfileActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }

}
