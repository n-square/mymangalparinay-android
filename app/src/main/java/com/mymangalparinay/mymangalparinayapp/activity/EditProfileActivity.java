package com.mymangalparinay.mymangalparinayapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;
import com.mymangalparinay.mymangalparinayapp.fragment.FragmentEditProfileContent;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 3/12/2017.
 */

public class EditProfileActivity extends AppCompatActivity {
    private static final String TAG = EditProfileActivity.class.getSimpleName();
    @BindView(R.id.ivProfileBack)
    ImageView ivProfileBack;
    @BindView(R.id.ivProfile)
    CircleImageView ivProfile;
    @BindView(R.id.ivEditImage)
    ImageView ivEditImage;
    @BindView(R.id.tvImageCount)
    CustomTextView tvImageCount;

    @BindView(R.id.appbar)
    Toolbar appbar;

    CustomTextView tvToolbarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this);
        loadToolBar();

        getSupportFragmentManager().beginTransaction().replace(R.id.flProfileContent, new FragmentEditProfileContent()).commit();

    }

    private void loadToolBar() {
        Log.d(TAG, "loadToolBar: ");
        setSupportActionBar(appbar);
        tvToolbarTitle = (CustomTextView) appbar.findViewById(R.id.tvToolbarTitle);
        ImageView ivBack = (ImageView) appbar.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, HomeActivity.class));
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();

    }
}
