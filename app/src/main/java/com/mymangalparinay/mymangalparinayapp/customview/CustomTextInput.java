package com.mymangalparinay.mymangalparinayapp.customview;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

import com.mymangalparinay.mymangalparinayapp.R;


public class CustomTextInput extends TextInputLayout {

    public CustomTextInput(Context context) {
        super(context);
        init(null);
    }

    public CustomTextInput(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CustomTextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    private void init(AttributeSet attrs) {
        // TODO Auto-generated method stub
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs,
                    R.styleable.CustomTextInput);
            String fontName = a
                    .getString(R.styleable.CustomTextInput_CustomTextInputFont);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext()
                        .getAssets(), "fonts/" + fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}
