package com.mymangalparinay.mymangalparinayapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterSent;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/30/2017.
 */

public class FragmentSent extends Fragment {
    private static final String TAG = FragmentSent.class.getSimpleName();
    View view;
    @BindView(R.id.lvSent)
    ListView lvSent;
    AdapterSent adapterSent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_send, container, false);
        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        adapterSent = new AdapterSent(getContext());
        lvSent.setAdapter(adapterSent);

    }
}
