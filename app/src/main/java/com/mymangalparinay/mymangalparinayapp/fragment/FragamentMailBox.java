package com.mymangalparinay.mymangalparinayapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterPager;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ADMIN on 3/30/2017.
 * 1
 */

public class FragamentMailBox extends Fragment {
    private static final String TAG = FragamentMailBox.class.getSimpleName();
    View view;
    @BindView(R.id.tabMail)
    TabLayout tabMail;
    @BindView(R.id.pagerMail)
    ViewPager pagerMail;

    AdapterPager adapterPager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragament_mail_box, container, false);
        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Mailbox");
        adapterPager = new AdapterPager(getChildFragmentManager());
        adapterPager.addFragment(new FragmentInbox(), "Received Message");
        adapterPager.addFragment(new FragmentSent(), "Sent Message");
        tabMail.setupWithViewPager(pagerMail);
        pagerMail.setAdapter(adapterPager);
    }
}
