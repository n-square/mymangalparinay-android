package com.mymangalparinay.mymangalparinayapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.activity.ActivityViewProfile;
import com.mymangalparinay.mymangalparinayapp.activity.ActivityViewPhoto;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterMatches;
import com.mymangalparinay.mymangalparinayapp.interfaces.OnClickDetailView;
import com.mymangalparinay.mymangalparinayapp.interfaces.OnClickViewPhoto;
import com.mymangalparinay.mymangalparinayapp.model.MatchesModel;
import com.mymangalparinay.mymangalparinayapp.utils.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/12/2017.
 * ,
 */

public class MatchesFragment extends Fragment implements OnClickDetailView, OnClickViewPhoto {
    private static final String TAG = MatchesFragment.class.getSimpleName();
    View view;
    @BindView(R.id.lvMatches)
    ListView lvMatches;

    AdapterMatches adapterMaches;

    List<MatchesModel> machesModelList;

    RequestQueue referenceQueue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_machaes, container, false);
        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("My Matches");

        machesModelList = new ArrayList<>();
        adapterMaches = new AdapterMatches(getActivity(), machesModelList);
        lvMatches.setAdapter(adapterMaches);
        adapterMaches.setOnClickProfile(this);
        adapterMaches.setOnClickPhoto(this);

        referenceQueue = Volley.newRequestQueue(getContext());
        //  loadDataFromServer();
        loadData();
    }

    private void loadDataFromServer() {
        Log.d(TAG, "loadDataFromServer: ");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.MATCHES_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("status");//": 1,
                    String msg = jsonObject.getString("msg");//": "success",
                    if (status.equalsIgnoreCase("0")) {
                        JSONArray data = jsonObject.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject dataObj = data.getJSONObject(i);
                            String id = dataObj.getString("id");//": "11",
                            String shortlist_flag = dataObj.getString("shortlist_flag");//": "0",
                            String interest_flag = dataObj.getString("interest_flag");//": "0",
                            String interest_messege = dataObj.getString("interest_messege");//": "",
                            String customerno = dataObj.getString("customerno");//": "M0011",
                            String name = dataObj.getString("name");//": "",
                            String age = dataObj.getString("age");//": "23",
                            String height = dataObj.getString("height");//": "-",
                            String education = dataObj.getString("education");//": "-",
                            String profile_img = dataObj.getString("profile_img");//": "http://localhost/mangal/webservices/profile_images/default-user.png"

                            //  MatchesModel matchesModel = new MatchesModel();
                            // matchesModel.setId(id);
                           /* matchesModel.setShortlist_flag(shortlist_flag);
                            matchesModel.setInterest_flag(interest_flag);
                            matchesModel.setInterest_messege(interest_messege);
                            matchesModel.setCustomerno(customerno);
                            matchesModel.setName(name);
                            matchesModel.setAge(age);
                            matchesModel.setHeight(height);
                            matchesModel.setEducation(education);
                            matchesModel.setProfile_img(profile_img);
*/

                            //  machesModelList.add(matchesModel);

                        }

                        adapterMaches.notifyDataSetChanged();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error);
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("customerid", "10");
                params.put("method", "fetch");
                params.put("startlimit", "0");
                params.put("limit", "5");
                return params;
            }
        };

        referenceQueue.add(stringRequest);

    }

    private void loadData() {
        machesModelList.add(new MatchesModel("MP0001", "Amol Bansode", "Software Engg", "24", "5.2", "Nasik", R.drawable.nitin));
        machesModelList.add(new MatchesModel("MP0002", "Sudrashan Ahire", "Prof.", "32", "5.5", "MAlegoan", R.drawable.person));
        machesModelList.add(new MatchesModel("MP0003", "Prajkata Mhaske", "Msc", "32", "5.5", "Aurangabad", R.drawable.img));
        machesModelList.add(new MatchesModel("MP0004", "Jayashri Sonawane", "ANM (Nurse)", "21", "5.5", "Nasik", R.drawable.person3));
        machesModelList.add(new MatchesModel("MP0005", "Pratibha Gangurde", "B.pharm", "21", "5.5", "Mumbai", R.drawable.person4));
        adapterMaches.notifyDataSetChanged();

    }

    @Override
    public void onDetailView(int posotion) {
        Log.d(TAG, "onDetailView:  posotion   :" + posotion);
        startActivity(new Intent(getActivity(), ActivityViewProfile.class));
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
        getActivity().finish();
    }

    @Override
    public void onClickViewPhoto(int posotion) {

        startActivity(new Intent(getActivity(), ActivityViewPhoto.class));
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
        getActivity().finish();

    }
}
