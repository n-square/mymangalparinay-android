package com.mymangalparinay.mymangalparinayapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.activity.ActivityViewProfile;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterMatches;
import com.mymangalparinay.mymangalparinayapp.interfaces.OnClickDetailView;
import com.mymangalparinay.mymangalparinayapp.model.MatchesModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/12/2017.
 * ,
 */

public class FragmentViewedMyProfile extends Fragment implements OnClickDetailView {
    private static final String TAG = FragmentViewedMyProfile.class.getSimpleName();
    View view;
    @BindView(R.id.lvMatches)
    ListView lvMatches;

    AdapterMatches adapterMaches;

    List<MatchesModel> machesModelList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_machaes, container, false);
        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Viewed My Profile");

        machesModelList = new ArrayList<>();
        adapterMaches = new AdapterMatches(getActivity(), machesModelList);
        lvMatches.setAdapter(adapterMaches);
        adapterMaches.setOnClickProfile(this);
        //loadData();
    }

  /*  private void loadData() {
        machesModelList.add(new MatchesModel("MP0001", "Amol Bansode", "Software Engg", "24", "5.2", "Nasik", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0002", "Sudrashan Ahire", "Prof.", "32", "5.5", "MAlegoan", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0003", "Prajkata Mhaske", "Msc", "32", "5.5", "Aurangabad", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0004", "Jayashri Sonawane", "ANM (Nurse)", "21", "5.5", "Nasik", R.drawable.ic_account_circle_black_24dp));
        machesModelList.add(new MatchesModel("MP0005", "Pratibha Gangurde", "B.pharm", "21", "5.5", "Mumbai", R.drawable.ic_account_circle_black_24dp));
        adapterMaches.notifyDataSetChanged();

    }
*/

    @Override
    public void onDetailView(int posotion) {
        Log.d(TAG, "onDetailView:  posotion   :" + posotion);
        startActivity(new Intent(getActivity(), ActivityViewProfile.class));
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

        getActivity().finish();
    }
}
