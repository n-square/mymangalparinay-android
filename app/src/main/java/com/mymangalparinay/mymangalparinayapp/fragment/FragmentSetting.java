package com.mymangalparinay.mymangalparinayapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.customview.CustomButtonView;
import com.mymangalparinay.mymangalparinayapp.customview.CustomEditTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 4/14/2017.
 */

public class FragmentSetting extends Fragment implements View.OnClickListener {
    View view;
    private static final String TAG = FragmentSetting.class.getSimpleName();

    @BindView(R.id.etOldPassword)
    CustomEditTextView etOldPassword;
    @BindView(R.id.etNewPassword)
    CustomEditTextView etNewPassword;
    @BindView(R.id.etConfirmPassword)
    CustomEditTextView etConfirmPassword;
    @BindView(R.id.bChangePassword)
    CustomButtonView bChangePassword;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_setting, container, false);
        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Setting");
        bChangePassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bChangePassword:
                doChangePassword();
                break;
        }


    }

    private void doChangePassword() {
        Log.d(TAG, "doChangePassword: ");


    }
}
