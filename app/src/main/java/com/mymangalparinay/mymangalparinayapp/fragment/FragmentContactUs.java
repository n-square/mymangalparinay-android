package com.mymangalparinay.mymangalparinayapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterSpinnerArea;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 4/12/2017.
 **/

public class FragmentContactUs extends Fragment {
    private static final String TAG = FragmentContactUs.class.getSimpleName();
    View view;

    @BindView(R.id.sArea)
    Spinner sArea;


    List<String> stringList;
    AdapterSpinnerArea adapterSpinnerArea;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Contact Us");

        stringList = new ArrayList<>();
        adapterSpinnerArea = new AdapterSpinnerArea(getContext(), stringList);
        sArea.setAdapter(adapterSpinnerArea);
        loadData();

    }

    private void loadData() {
        Log.d(TAG, "loadData: ");
        stringList.add("Select Area");
        stringList.add("Amabd");
        stringList.add("Satpur");
        stringList.add("DGP Nagar-1");
        stringList.add("Nasik-road");
        stringList.add("Manmad");
        stringList.add("Ganagpur-road");
        stringList.add("Masarul");
        adapterSpinnerArea.notifyDataSetChanged();
    }
}
