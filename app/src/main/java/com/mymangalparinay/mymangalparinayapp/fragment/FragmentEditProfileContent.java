package com.mymangalparinay.mymangalparinayapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.activity.EditAboutMeActivity;
import com.mymangalparinay.mymangalparinayapp.activity.EditBasicDetailsActivity;
import com.mymangalparinay.mymangalparinayapp.activity.EditContactInfoActivity;
import com.mymangalparinay.mymangalparinayapp.activity.EditFamilyDetailsActivity;
import com.mymangalparinay.mymangalparinayapp.activity.EditPreferencesActivity;
import com.mymangalparinay.mymangalparinayapp.activity.EditProfessionalDetailsActivity;
import com.mymangalparinay.mymangalparinayapp.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 3/18/2017.
 */

public class FragmentEditProfileContent extends Fragment implements View.OnClickListener {
    private static final String TAG = FragmentEditProfileContent.class.getSimpleName();
    View view;
    @BindView(R.id.tvAboutMe)
    CustomTextView tvAboutMe;
    @BindView(R.id.tvName)
    CustomTextView tvName;
    @BindView(R.id.tvMiddleName)
    CustomTextView tvMiddleName;
    @BindView(R.id.tvLastName)
    CustomTextView tvLastName;
    @BindView(R.id.tvGender)
    CustomTextView tvGender;
    @BindView(R.id.tvAge)
    CustomTextView tvAge;
    @BindView(R.id.tvHeight)
    CustomTextView tvHeight;
    @BindView(R.id.tvWeight)
    CustomTextView tvWeight;
    @BindView(R.id.tvMaritalStatus)
    CustomTextView tvMaritalStatus;
    @BindView(R.id.tvMotherTongue)
    CustomTextView tvMotherTongue;
    @BindView(R.id.tvPhysicalStatus)
    CustomTextView tvPhysicalStatus;
    @BindView(R.id.tvBodyType)
    CustomTextView tvBodyType;
    @BindView(R.id.tvComplexion)
    CustomTextView tvComplexion;
    @BindView(R.id.tvEatingHabits)
    CustomTextView tvEatingHabits;
    @BindView(R.id.tvDrinkingHabits)
    CustomTextView tvDrinkingHabits;
    @BindView(R.id.tvSmokingHabits)
    CustomTextView tvSmokingHabits;
    @BindView(R.id.tvPrimaryNumber)
    CustomTextView tvPrimaryNumber;
    @BindView(R.id.tvSecondaryNumber)
    CustomTextView tvSecondaryNumber;
    @BindView(R.id.tvHomeAddress)
    CustomTextView tvHomeAddress;
    @BindView(R.id.tvEmail)
    CustomTextView tvEmail;
    @BindView(R.id.tvFatherFullName)
    CustomTextView tvFatherFullName;
    @BindView(R.id.tvMotherFullName)
    CustomTextView tvMotherFullName;
    @BindView(R.id.tvFamilyValue)
    CustomTextView tvFamilyValue;
    @BindView(R.id.tvFamilyType)
    CustomTextView tvFamilyType;
    @BindView(R.id.tvFatherOccupation)
    CustomTextView tvFatherOccupation;
    @BindView(R.id.tvNoOfBrother)
    CustomTextView tvNoOfBrother;
    @BindView(R.id.tvNoOfBrotherMarriage)
    CustomTextView tvNoOfBrotherMarriage;
    @BindView(R.id.tvNoOfSisterMarriage)
    CustomTextView tvNoOfSisterMarriage;
    @BindView(R.id.tvNoOfSister)
    CustomTextView tvNoOfSister;
    @BindView(R.id.tvWhatImLookingForPreference)
    CustomTextView tvWhatImLookingForPreference;
    @BindView(R.id.tvHighestEducation)
    CustomTextView tvHighestEducation;
    @BindView(R.id.tvCollegeInstitution)
    CustomTextView tvCollegeInstitution;
    @BindView(R.id.tvEducationDetails)
    CustomTextView tvEducationDetails;
    @BindView(R.id.tvOccupation)
    CustomTextView tvOccupation;
    @BindView(R.id.tvOccupationDetails)
    CustomTextView tvOccupationDetails;
    @BindView(R.id.tvEmployedIn)
    CustomTextView tvEmployedIn;
    @BindView(R.id.tVAnnualIncomeType)
    CustomTextView tVAnnualIncomeType;
    @BindView(R.id.tvAnnualIncome)
    CustomTextView tvAnnualIncome;
    @BindView(R.id.tvDesignation)
    CustomTextView tvDesignation;


    // CustomTextView tvAboutMe;


    /* ImageView ivAbout
     ImageView ivBasic
     ImageView imageViewl
     ImageView imageViewl
     ImageView imageViewl
     ImageView imageViewl

 */
    @BindView(R.id.rlIconBasic)
    RelativeLayout rlIconBasic;
    @BindView(R.id.rlIconAbout)
    RelativeLayout rlIconAbout;
    @BindView(R.id.rlIconProf)
    RelativeLayout rlIconProf;
    @BindView(R.id.rlIconContact)
    RelativeLayout rlIconContact;
    @BindView(R.id.rlIconFamily)
    RelativeLayout rlIconFamily;
    @BindView(R.id.rlIconSearch)
    RelativeLayout rlIconSearch;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit_profile_content, container, false);
        init();

        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        rlIconBasic.setOnClickListener(this);
        rlIconAbout.setOnClickListener(this);
        rlIconProf.setOnClickListener(this);
        rlIconContact.setOnClickListener(this);
        rlIconFamily.setOnClickListener(this);
        rlIconSearch.setOnClickListener(this);

        setData();

    }

    private void setData() {
        Log.d(TAG, "setData: ");
        tvAboutMe.setText("nice");
        tvName.setText("Deeepak");
        tvMiddleName.setText("Laxman");
        tvLastName.setText("Pnachal");
        tvGender.setText("Male");
        tvAge.setText("23 years /20-12-1993 ");
        tvHeight.setText("5.2''");
        tvWeight.setText("52kgs");
        tvMaritalStatus.setText("never married");
        tvMotherTongue.setText("marathi");
        tvPhysicalStatus.setText("no changes");
        tvBodyType.setText("normal");
        tvComplexion.setText("test");
        tvEatingHabits.setText("non veg");
        tvDrinkingHabits.setText("never");
        tvSmokingHabits.setText("never");
        tvPrimaryNumber.setText("9096693518");
        tvSecondaryNumber.setText("8855862127");
        tvHomeAddress.setText("amabd");
        tvEmail.setText("d@gamil.coom");
        tvFatherFullName.setText("Laxman panchal");
        tvMotherFullName.setText("b panchal");
        tvFamilyValue.setText("fv test");
        tvFamilyType.setText("ft test");
        tvFatherOccupation.setText("engg.");
        tvNoOfBrother.setText("1");
        tvNoOfBrotherMarriage.setText("1");
        tvNoOfSisterMarriage.setText("1");
        tvNoOfSister.setText("2");
        tvWhatImLookingForPreference.setText("sutable");

        tvHighestEducation.setText("BCA");
        tvCollegeInstitution.setText("KK wagh");
        tvEducationDetails.setText("comp engg");
        tvOccupation.setText("Softwere Eng");
        tvOccupationDetails.setText("Android Developer");
        tvEmployedIn.setText("private");
        tVAnnualIncomeType.setText("Annual");
        tvAnnualIncome.setText("80000");
        tvDesignation.setText("Senior Devl.");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlIconBasic:
                startActivity(new Intent(getActivity(), EditBasicDetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                getActivity().finish();
                break;
            case R.id.rlIconAbout:
                startActivity(new Intent(getActivity(), EditAboutMeActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                getActivity().finish();
                break;
            case R.id.rlIconProf:
                startActivity(new Intent(getActivity(), EditProfessionalDetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                getActivity().finish();
                break;
            case R.id.rlIconContact:
                startActivity(new Intent(getActivity(), EditContactInfoActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                getActivity().finish();
                break;
            case R.id.rlIconFamily:
                startActivity(new Intent(getActivity(), EditFamilyDetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                getActivity().finish();
                break;
            case R.id.rlIconSearch:
                startActivity(new Intent(getActivity(), EditPreferencesActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                getActivity().finish();
                break;
        }


    }
}
