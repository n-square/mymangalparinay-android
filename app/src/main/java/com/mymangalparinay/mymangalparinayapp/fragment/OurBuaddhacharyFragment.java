package com.mymangalparinay.mymangalparinayapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterBuddhachary;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 4/7/2017.
 */

public class OurBuaddhacharyFragment extends Fragment {
    private static final String TAG = OurBuaddhacharyFragment.class.getSimpleName();
    View view;
    AdapterBuddhachary adapterBuddhachary;

    @BindView(R.id.lvBuchary)
    ListView lvBuchary;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_our_buaddhachary, container, false);

        init();
        return view;
    }

    private void init() {
        Log.d(TAG, "init: ");
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Our Buddhacharyas");
        adapterBuddhachary = new AdapterBuddhachary(getContext());
        lvBuchary.setAdapter(adapterBuddhachary);
    }
}
