package com.mymangalparinay.mymangalparinayapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mymangalparinay.mymangalparinayapp.R;
import com.mymangalparinay.mymangalparinayapp.adapter.AdapterCommunityBusinessmen;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 4/8/2017.
 */

public class FragmentCommunityBusinessmen extends Fragment {
    private static final String TAG = FragmentCommunityBusinessmen.class.getSimpleName();
    View view;

    @BindView(R.id.lvCommity)
    ListView lvCommity;

    AdapterCommunityBusinessmen adapterCommunityBusinessmen;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_community_businessmen, container, false);
        init();
        return view;

    }

    private void init() {
        Log.d(TAG, "init: invoke");
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Community Businessmen");
        adapterCommunityBusinessmen = new AdapterCommunityBusinessmen(getContext());
        lvCommity.setAdapter(adapterCommunityBusinessmen);
    }


}
