package com.mymangalparinay.mymangalparinayapp.model;

/**
 * Created by user on 07-Dec-16.
 */
public class NavItemModel {
    String navItemName;

    public NavItemModel(String navItemName) {
        this.navItemName = navItemName;
    }

    public String getNavItemName() {
        return navItemName;
    }

    public void setNavItemName(String navItemName) {
        this.navItemName = navItemName;
    }
}
