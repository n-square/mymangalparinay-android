package com.mymangalparinay.mymangalparinayapp.model;

/**
 * Created by ADMIN on 4/1/2017.
 */

public class MatchesModel {
    String id;
    String name;
    String edu;
    String age;
    String height;
    String address;
    int photo;

    public MatchesModel(String id, String name, String edu, String age, String height, String address, int photo) {
        this.id = id;
        this.name = name;
        this.edu = edu;
        this.age = age;
        this.height = height;
        this.address = address;
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }


  /*  private String id;
    private String shortlist_flag;
    private String interest_flag;
    private String interest_messege;
    private String customerno;
    private String name;
    private String age;
    private String height;
    private String education;
    private String profile_img;

    public MatchesModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortlist_flag() {
        return shortlist_flag;
    }

    public void setShortlist_flag(String shortlist_flag) {
        this.shortlist_flag = shortlist_flag;
    }

    public String getInterest_flag() {
        return interest_flag;
    }

    public void setInterest_flag(String interest_flag) {
        this.interest_flag = interest_flag;
    }

    public String getInterest_messege() {
        return interest_messege;
    }

    public void setInterest_messege(String interest_messege) {
        this.interest_messege = interest_messege;
    }

    public String getCustomerno() {
        return customerno;
    }

    public void setCustomerno(String customerno) {
        this.customerno = customerno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }*/
}
