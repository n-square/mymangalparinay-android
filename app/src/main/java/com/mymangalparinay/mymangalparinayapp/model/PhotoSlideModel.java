package com.mymangalparinay.mymangalparinayapp.model;

/**
 * Created by Jyoti on 10/1/2017.
 */

public class PhotoSlideModel {

    int image;

    public PhotoSlideModel(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
