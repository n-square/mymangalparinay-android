package com.mymangalparinay.mymangalparinayapp.utils;

/**
 * Created by Akash on 5/28/2017.
 * 0
 */

public class Config {
    /* private static final String BASE_URL = "http://localhost/mangal/webservices/";*/
    private static final String BASE_URL = "http://127.0.0.1//mangal/webservices/";
    public static final String REGISTER = BASE_URL + "registration-one.php";
    public static final String LOGIN = BASE_URL + "login.php";
    public static final String MATCHES_LIST = BASE_URL + "suggestion-list.php";


}
